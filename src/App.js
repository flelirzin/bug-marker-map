import React, {
  useState,
  useEffect,
  useLayoutEffect,
  useContext,
  useReducer,
  useRef
} from "react";

import "./App.css";

import { clone, find, get, keys } from "lodash";

import {
  GoogleMap,
  useJsApiLoader,
  Marker,
  InfoWindow,
  KmlLayer,
  LoadScript
} from "@react-google-maps/api";

import printf from "printf";

import stringify from "json-stable-stringify";

import { mapApiKey } from "./mapApiKey";

const strify = data => stringify(data, { space: 4 });

const libraries = ["drawing"];

const ctr = {
  lat: 48.85,
  lng: 2.35
};

const gap = 0.04;

const markers = [
  {
    id: "m1",
    geo: { lat: ctr.lat + gap, lng: ctr.lng + gap },
    iconName: "site",
    color: "#f80"
  },
  {
    id: "m2",
    geo: { lat: ctr.lat - gap, lng: ctr.lng - gap },
    iconName: "site",
    color: "#f00"
  },
  {
    id: "m3",
    geo: { lat: ctr.lat - gap, lng: ctr.lng + gap },
    iconName: "valve",
    color: "#0cf"
  },
  {
    id: "m4",
    geo: { lat: ctr.lat + gap, lng: ctr.lng - gap },
    iconName: "valve",
    color: "#80f"
  }
];

const selectedColor = "#444";

const svgIcon = (color, opacity) => {
  const rv =
    '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88.96 85.05" ' +
    (opacity !== undefined ? 'fillOpacity="' + opacity + '"' : "") +
    '><g color="' +
    color +
    '" id="Layer_2" data-name="Layer 2"><path d="M270,438a41.45,41.45,0,0,1-18.41-8.2c-2.19-1.75-6.64-.9-10-.82-1.73,0-3.44,1-5.54,1.64V397.09c1.66.53,3,1.28,4.36,1.33,3.74.12,8.38,1.3,11-.44,3.75-2.46,7.25-2.79,11.23-2.77,6.16,0,12.33,0,18.5,0a17.54,17.54,0,0,1,10.25,2.67c2.64,1.66,7,.83,10.55.75,1.57,0,3.12-1,5.07-1.62v33.48c-2.81-.73-5.41-1.78-7.91-1.9-3.14-.15-6.53,1.45-9.66,2.61-5.63,2.08-6.46,5.56-12.67,6.8Z" transform="translate(-227.12 -352.95)" fill="currentColor"/><path d="M271.52,353H308c2.76,0,5.8,0,6,3.62.2,4.1-3.13,4.1-6,4.11q-36.78,0-73.55,0c-2.84,0-6-.44-5.22-4.08.33-1.53,3.39-3.45,5.25-3.49C246.83,352.83,259.18,353,271.52,353Z" transform="translate(-227.12 -352.95)" fill="currentColor"/><path d="M227.12,391.12h6.5v45.6h-6.5Z" transform="translate(-227.12 -352.95)" fill="currentColor"/><path d="M316.08,436.75h-6.49V391.29h6.49Z" transform="translate(-227.12 -352.95)" fill="currentColor"/><path d="M257.29,386.58h28.6v6.56h-28.6Z" transform="translate(-227.12 -352.95)" fill="currentColor"/><path d="M259,370.81v-5.46h24.83v5.46Z" transform="translate(-227.12 -352.95)" fill="currentColor"/><path d="M266.28,383.53V374h10.51v9.51Z" transform="translate(-227.12 -352.95)" fill="currentColor"/></g></svg>';

  // console.log(`rv / ${rv}`);

  return rv;
};

const svgTarget = (color, opacity) => {
  const rv =
    '<svg  xmlns="http://www.w3.org/2000/svg" focusable="false" viewBox="0 0 512 512" color="' +
    color +
    '" ' +
    (opacity !== undefined ? 'fillOpacity="' + opacity + '"' : "") +
    '><path fill="currentColor" d="M504 240h-56.81C439.48 146.76 365.24 72.52 272 64.81V8c0-4.42-3.58-8-8-8h-16c-4.42 0-8 3.58-8 8v56.81C146.76 72.52 72.52 146.76 64.81 240H8c-4.42 0-8 3.58-8 8v16c0 4.42 3.58 8 8 8h56.81c7.71 93.24 81.95 167.48 175.19 175.19V504c0 4.42 3.58 8 8 8h16c4.42 0 8-3.58 8-8v-56.81c93.24-7.71 167.48-81.95 175.19-175.19H504c4.42 0 8-3.58 8-8v-16c0-4.42-3.58-8-8-8zM256 416c-88.22 0-160-71.78-160-160S167.78 96 256 96s160 71.78 160 160-71.78 160-160 160zm0-256c-53.02 0-96 42.98-96 96s42.98 96 96 96 96-42.98 96-96-42.98-96-96-96zm0 160c-35.29 0-64-28.71-64-64s28.71-64 64-64 64 28.71 64 64-28.71 64-64 64z" ></path></svg>';

  // console.log(`rv / ${rv}`);

  return rv;
};

const googleMapDefaultOptions = {
  fullscreenControl: false,
  streetViewControl: false,
  mapTypeControl: false,
  zoomControl: true,
  clickableIcons: false,
  styles: [
    // {
    //   featureType: "poi",
    //   elementType: "",
    //   stylers: [{ color: "#ffffff" }]
    // },
    // {
    //   featureType: "landscape",
    //   elementType: "",
    //   stylers: [{ color: "#ffffff" }]
    // },
    // {
    //   featureType: "water",
    //   elementType: "",
    //   stylers: [{ color: "#ccccff" }]
    // },
    // {
    //   featureType: "road",
    //   elementType: "",
    //   stylers: [{ color: "#ffffff" }]
    // }
  ]
};

const containerStyle = {
  width: "1000px",
  height: "1000px"
};

const MapCpn = props => {
  const [isLoaded, isLoadedSet] = useState(false);

  useEffect(() => {
    if (!isLoaded) {
      new Promise((res, rej) => {
        setTimeout(() => {
          console.log(`window.google ${window.google ? true : false}`);

          if (window.google) {
            isLoadedSet(true);
          }
          res();
        }, 1000);
      });
    }
  }, []);

  if (!isLoaded) {
    return <div>Not loaded</div>;
  }

  return (
    <GoogleMap
      center={ctr}
      zoom={11}
      mapContainerStyle={{
        height: "100%",
        width: "100%"
      }}
      options={googleMapDefaultOptions}
    >
      {markers.map(marker => {
        return (
          <Marker
            position={marker.geo}
            key={marker.id}
            onClick={evt => {
              // console.log(`clicked ${marker.id}`, evt);
              // evt.stop();
              if (!props.disableAction) {
                props.selectedMarkerSet(marker.id);
                props.dbgLineSet(
                  `${marker.id} / lat ${printf(
                    "%5.2f",
                    marker.geo.lat
                  )} / lng ${printf("%5.2f", marker.geo.lng)}`
                );
                // if (marker && marker.ref) {
                //   const zindex = marker.ref.getZIndex();
                //   console.log(`z-index ${zindex}`);
                // }
              }
            }}
            label={marker.id}
            icon={{
              url:
                "data:image/svg+xml;charset=UTF-8;base64," +
                btoa(
                  props.selectedMarker === marker.id
                    ? svgTarget(selectedColor, 1)
                    : svgIcon(
                        props.selectedMarker === marker.id
                          ? selectedColor
                          : marker.color
                      )
                ),
              scaledSize: new window.google.maps.Size(100, 100),
              origin: new window.google.maps.Point(0, 0),
              anchor: new window.google.maps.Point(50, 50)
            }}
            onLoad={ref => {
              // console.log(`set ref for marker ${marker.id}`);
              marker.ref = ref;
            }}
            options={{ zIndex: props.selectedMarker === marker.id ? 200 : 100 }}
            opacity={
              props.selectedMarker === marker.id
                ? props.inverted
                  ? 1
                  : 0.4
                : props.inverted
                ? 0.4
                : 1
            }
          ></Marker>
        );
      })}
    </GoogleMap>
  );
};

function App() {
  const [dbgLine, dbgLineSet] = useState("");
  const [disableAction, disableActionSet] = useState(false);
  const [selectedMarker, selectedMarkerSet] = useState(null);
  const [viewPath, viewPathSet] = useState([]);
  const [mapRef, mapRefSet] = React.useState(null);
  const [inverted, invertedSet] = React.useState(false);

  return (
    <div className="App">
      <LoadScript
        googleMapsApiKey={mapApiKey}
        libraries={libraries}
        version="3.43.8"
      >
        <MapCpn
          dbgLineSet={dbgLineSet}
          disableAction={disableAction}
          selectedMarker={selectedMarker}
          selectedMarkerSet={selectedMarkerSet}
          inverted={inverted}
        />
      </LoadScript>
      <div className="dbg">{dbgLine}</div>
      <div className="enable-btn">
        <button onClick={evt => disableActionSet(!disableAction)}>
          {disableAction ? "disabled" : "enabled"}
        </button>
      </div>
      <div className="inverted-btn">
        <button onClick={evt => invertedSet(!inverted)}>
          {inverted ? "new" : "existing"}
        </button>
      </div>
    </div>
  );
}

export default App;
